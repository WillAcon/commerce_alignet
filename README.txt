Drupal commerce_payulatam module:
------------------------
Maintainers:
  Williams Acuña (https://www.drupal.org/u/willacon)
Requires - Drupal 7
License - GPL (see LICENSE)



Configuration:
-------------
Go to "Store" -> "Configuration" -> "Payment methods" ->  to find
all the configuration options.

Reports:
---------------------
admin/commerce_alignet/report

