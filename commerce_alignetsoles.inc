<?php
/**
 * Page response.
 */
function commerce_alignetsoles_response($order, $token) {
 // dpm($_POST);
  if($_POST) {
      $param = $_POST;
      //$param = variables_to_save_order_payment($_POST)
      $order_id = $_POST['purchaseOperationNumber'];
      $order = commerce_order_load($order_id);
      unset($param['javax_faces_ViewState']);
      commerce_alignetsoles_confirmation($order, $param);
      drupal_set_title($param['errorMessage']);
      $rows = array(
        array(t('AuthorizationResult'), $param['authorizationResult']),
        array(t('AuthorizationCode'), $param['authorizationCode']),
        array(t('ErrorCode'), $param['errorCode']),
        array(t('Bin'), $param['bin']),
        array(t('Brand'), $param['brand']),
        array(t('PaymentReferenceCode'), $param['paymentReferenceCode']),
        array(t('Reserved1'), $param['reserved1']),
        array(t('Número de Operacion'), $param['purchaseOperationNumber']),
        array(t('Descripción'), $param['descriptionProducts']),
        array(t('Monto'), $param['purchaseAmount']/100),
      );
  }
  else {
      return drupal_not_found();
  }
  $content['table'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
  );
  $arguments = array(
    'message' => $payload['mensaje_respuesta_usuario'],
  );
  $content = drupal_render($content);
  $form = drupal_render(drupal_get_form('commerce_alignetsoles_cancel_form', $arguments));
  $content = $content.$form;
  return $content;
}

/**
 * Page confirmation.
 */
function commerce_alignetsoles_confirmation($order, $param) {
    $payments = array();
    $payments = commerce_payment_transaction_load_multiple(array(), array('order_id' => $order->order_id));
    if(count($payments) <= 0) {
         //$param = commerce_alignetsoles_json_param($order);
        if($param['authorizationResult']) {
          $transaction = commerce_alignetsoles_save_transation($param, $order);
          $payment = commerce_payment_method_instance_load($order->data['payment_method']);
          $default_status = array(
            COMMERCE_PAYMENT_STATUS_SUCCESS => 'pending',
            COMMERCE_PAYMENT_STATUS_FAILURE => 'canceled',
          );
          if($transaction) {
              if (!isset($payment['settings']['py_assign_status']) || $payment['settings']['py_assign_status'] == 'M') {
                 $status = $default_status[$transaction->status];
                 if (isset($payment['settings']['py_status_' . $transaction->status])) {
                    $status = $payment['settings']['py_status_' . $transaction->status];
                 }
                 commerce_order_status_update($order, $status);
                 if ($transaction->status == COMMERCE_PAYMENT_STATUS_SUCCESS) {
                    commerce_checkout_complete($order);
                    $result['message'] = 'success';
                 }
                 else {
                     //commerce_order_status_update($order, 'canceled');
                     $result['message'] = 'failed';
                 }
              }
              elseif ($transaction->status == COMMERCE_PAYMENT_STATUS_SUCCESS) {
                 commerce_payment_redirect_pane_next_page($order);
                 $result['message'] = 'success';
              }
              else {
                 commerce_order_status_update($order, 'canceled');
                 $result['message'] = 'failed';
              }
            }
            else {
                $result['alert_message'] = $param['mensaje_respuesta_usuario'];
                $result['message'] = 'alert';
            }
            module_invoke_all('commerce_alignetsoles_confirmation', $order, $transaction);
        }
    }
}


/**
 * Save transaction.
 */
function commerce_alignetsoles_save_transation($param, $order) {
  $amount = $param['purchaseAmount'];
 // $amount = commerce_currency_decimal_to_amount($exchange_money, $param['currency_code']);

  //$amount = number_format(commerce_currency_amount_to_decimal($param['amount'], $param['currency_code']), 2, '.', '');
  $payment = commerce_payment_method_instance_load($order->data['payment_method']);
  $param['instance_id'] = $payment['instance_id'];

 if($param['authorizationResult'] == RESPONSE_ALIGNET_SUCCESS) {
      $result = db_insert('commerce_alignetsoles')
          ->fields(array(
            'order_id' => $order->order_id,
            'created' => REQUEST_TIME,
            'reference_culqi' => isset($param['authorizationCode']) ? $param['authorizationCode'] : 'NN',
            'state_transaction' => isset($param['commerceMallId']) ? $param['commerceMallId'] : 'NN',
            'card_brand' => isset($param['brand']) ? $param['brand'] : 'NN',//status order
            'value' => commerce_currency_amount_to_decimal($amount, $param['reserved1']),
            'response' => serialize($param),
          ))
          ->execute();
 }

  $transaction = commerce_payment_transaction_new('alignetsoles_payment', $order->order_id);
  $transaction->payload[REQUEST_TIME] = $param;
  $transaction->remote_id = $param['id_transaccion'];//faLTA
  $transaction->remote_status = $param['authorizationResult'];

  switch ($param['authorizationResult']) {
    case RESPONSE_ALIGNET_SUCCESS:
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      break;
    default:
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
  }
  $transaction->instance_id = $param['instance_id'];
  $transaction->amount = $amount;
  $transaction->currency_code = $param['reserved1'];
  $transaction->message = $param['answerMessage'];

  if($param['authorizationResult'] != RESPONSE_ALIGNET_SUCCESS) {
      return NULL;
   }
  commerce_payment_transaction_save($transaction);

  return $transaction;
}



/**
 * Generate wallet reference unique.
 */
function commerce_alignetsoles_wallet_consult($settings, $order, $address) {

  $registerVerification = openssl_digest($settings['py_idEntCommerce'] . $settings['py_codCardHolderCommerce'] . $order->mail . $settings['py_wallet_secrectkey'], 'sha512');
  //return $alias . $order_id;
    //$wsdl = $settings['py_wallet_url_test'];

    $wsdl = $settings['py_wallet_url_production'];
    if($settings['py_testing'] == 1) {
      $wsdl = $settings['py_wallet_url_test'];
    }

    $client = new SoapClient($wsdl);
    //Creación de Arreglo para el almacenamiento y envío de parametros.
    $params = array(
        'idEntCommerce' => $settings['py_idEntCommerce'],
        'codCardHolderCommerce' => $settings['py_codCardHolderCommerce'],
        'names' => $address['first_name'],
        'lastNames' => $address['last_name'],
        'mail' => $order->mail,
        'registerVerification' => $registerVerification
    );

    //Consumo del metodo RegisterCardHolder
    $result = $client->RegisterCardHolder($params);

    return $result;
}



/**
 * Return transaction data
 */
function commerce_alignetsoles_load_transaction($order_id) {
   $payments = commerce_payment_transaction_load_multiple(array(), array('order_id' => $order_id));
   $payment = !empty($payments) ? array_shift($payments) : NULL;
   $id_pay = $payment->created;
   $payload = $payment->payload[$id_pay];
   return $payload;
}

/**
 *
 */
function validate_culqi_data($data) {
  $validate = TRUE;
  foreach ($data as $key => $value) {
    size_field_culqi($key, $value, $validate);
    if(empty($value) && $key != 'iduser') {
     // dpm($data);
      if($key == 'City' && $data['country'] == 'PE') {
         drupal_set_message(t('Please fill the field department and district'), 'error');
      }
      else {
        drupal_set_message(t($key).' '.t('field  is required'), 'error');
      }
      $validate = FALSE;
    }
  }
  return $validate;
}

/**
 * Validate fields 
 */
function size_field_culqi($key, $field, &$validate) {
  $limit = array(
    'Name' => array(
      'limit' => 2,
      'message' => t('Ingrese Nombres válidos'),
     ),
    'Lastname' => array(
      'limit' => 2,
      'message' => t('Ingrese Apellidos válidos'),
     ),
    'Address' => array(
      'limit' => 5,
      'message' => t('Ingrese una dirección válida'),
     ),
  );
  if(isset($field) && isset($limit[$key]['limit']) && strlen($field) < $limit[$key]['limit']) {
      drupal_set_message($limit[$key]['message'], 'warning');
      $validate = FALSE;
  }
  if($key == 'Name' || $key == 'Lastname') {
    if(strlen(preg_replace("/[^a-zA-Z]/ ", "", $field)) < $limit[$key]['limit']) {
        drupal_set_message($limit[$key]['message'], 'warning');
        $validate = FALSE;
    }
  }
}